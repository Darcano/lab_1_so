//Clase del Sistema Opertativo
class So {
  constructor(kernel, stack, heap, encabezado) {
    this.kernel = kernel;
    this.stack = stack;
    this.heap = heap;
    this.encabezado = encabezado;
  }
}

//Funcion conversora
function to_bytes(tam) {
  if (tam.unidad === "MiB") {
    return { unidad: "bytes", valor: tam.valor * 1024 * 1024 };
  } else if (tam.unidad === "KiB") {
    return { unidad: "bytes", valor: tam.valor * 1024 };
  } else if (tam.unidad === "bytes") {
    return { unidad: "bytes", valor: tam.valor };
  }
}

//Clase de proceso
class Proceso {
  constructor(pid, nombre, text, data, bss, SistemO, particion) {
    this.pid = pid;
    this.nombre = nombre;
    this.text = to_bytes(text);
    this.data = to_bytes(data);
    this.bss = to_bytes(bss);
    const tamDisco =
      this.text.valor +
      this.data.valor +
      this.bss.valor +
      to_bytes(SistemO.encabezado).valor;
    this.disco = { unidad: "bytes", valor: tamDisco };
    const memoIni = {
      unidad: "bytes",
      valor:
        this.text.valor +
        this.data.valor +
        this.bss.valor +
        to_bytes(SisO.encabezado).valor +
        to_bytes(SisO.heap).valor +
        to_bytes(SisO.stack).valor,
    };
    this.memoInicial = memoIni;
    this.particion = particion;
  }
}

//Clase de la RAM
class Ram {
  constructor(tamParti, tamRam) {
    this.tam = tamParti;
    this.cant = tamParti.length;
    this.tamRam = tamRam;
  }
}

//Clase de la particion
class Particion {
  constructor(id, tam, estado) {
    this.id = id;
    this.tam = to_bytes(tam);
    this.estado = estado;
  }
}

//Objeto del sistema operativo
const SisO = new So(
  { unidad: "MiB", valor: 1 },
  { unidad: "KiB", valor: 64 },
  { unidad: "KiB", valor: 128 },
  { unidad: "bytes", valor: 180 }
);

//Objetos de los procesos
const proNote = new Proceso(
  "p1",
  "Notepad",
  { unidad: "bytes", valor: 18654 },
  { unidad: "bytes", valor: 10352 },
  { unidad: "bytes", valor: 164 },
  SisO,
  ""
);
const proWord = new Proceso(
  "p2",
  "Word",
  { unidad: "bytes", valor: 80000 },
  { unidad: "bytes", valor: 10000 },
  { unidad: "bytes", valor: 100 },
  SisO,
  ""
);
const proExcel = new Proceso(
  "p3",
  "Excel",
  { unidad: "bytes", valor: 99542 },
  { unidad: "bytes", valor: 12500 },
  { unidad: "bytes", valor: 500 },
  SisO,
  ""
);
const proAuto = new Proceso(
  "p4",
  "AutoCAD",
  { unidad: "bytes", valor: 115000 },
  { unidad: "bytes", valor: 123470 },
  { unidad: "bytes", valor: 1123 },
  SisO,
  ""
);
const proCalc = new Proceso(
  "p5",
  "Calculadora",
  { unidad: "bytes", valor: 15386 },
  { unidad: "bytes", valor: 256 },
  { unidad: "bytes", valor: 256 },
  SisO,
  ""
);

let listProceCreate = [proNote, proWord, proExcel, proAuto, proCalc]; //Lista de procesos creados
let listParticiones = []; //Lista de Particiones

function getProceso(pid, listaProcesos) {
  let proceso;
  listaProcesos.forEach((element) => {
    if (pid == element.pid) {
      proceso = element;
    }
  });

  return proceso;
}

const tamRam = to_bytes({ unidad: "MiB", valor: 16 }); //Tamaño de la RAM
const tamPart = [{ unidad: "MiB", valor: 16 }]; //Tamaño de las particiones

const bytes_tamPart = [];

tamPart.forEach( e => bytes_tamPart.push(to_bytes(e)));


//Objeto de la RAM
const ram = new Ram(bytes_tamPart, tamRam.valor);

//Funcion para dibujar las particiones de la RAM
function dibujarRam() {
  const ramDibu = document.getElementById("dibujoDeLaRam");

  for (let index = 0; index < ram.cant; index++) {
    const div = document.createElement("div");
    div.id = "particion_" + index;
    div.setAttribute("class", "particiones");
    div.style.minHeight = ((700*ram.tam[index].valor)/(ram.tamRam)) + "px";
    div.style.maxHeight = ((700*ram.tam[index].valor)/(ram.tamRam)) + "px";

    const divPos = document.createElement("div");
    divPos.setAttribute("class", "posicionRam");

    div.appendChild(divPos);

    ramDibu.appendChild(div);

    let particion = new Particion(index, tamPart[index], true);
   
    listParticiones.push(particion);
  
  }
}

dibujarRam();

function addPart(proceso, index){
  var particiones = document.querySelectorAll(".particiones");
  const div = document.getElementById('dibujoDeLaRam');
  part = new Particion(index,proceso.memoInicial,true);
  mover = listParticiones[index];
  mover.id++;
  mover.tam.valor -= proceso.memoInicial.valor; 
  listParticiones.splice(index+1,0,mover)
  listParticiones[index] = part
  ram.cant++;
  particiones.forEach((e)=> div.removeChild(e));

  mover2 = bytes_tamPart[index];
  bytes_tamPart.splice(index+1,0,mover2);
  bytes_tamPart[index] = proceso.memoInicial;
  ram.tam = bytes_tamPart;

  const ramDibu = document.getElementById("dibujoDeLaRam");

  for (let index = 0; index < ram.cant; index++) {
    const div = document.createElement("div");
    div.id = "particion_" + index;
    div.setAttribute("class", "particiones");
    div.style.minHeight = ((700*ram.tam[index].valor)/(ram.tamRam)) + "px";
    div.style.maxHeight = ((700*ram.tam[index].valor)/(ram.tamRam)) + "px";

    const divPos = document.createElement("div");
    divPos.setAttribute("class", "posicionRam");

    div.appendChild(divPos);

    ramDibu.appendChild(div);
    
    if (listParticiones[index].estado === false){
      const div2 = document.createElement("div");
      div2.id = "particion_memoria_"+listParticiones[index];
      div2.setAttribute("class", "procesoRam");
      pro_actual = NaN;
      listProceCreate .forEach((p)=>{
        if (p.particion === listParticiones[index].id){
          pro_actual = p;
        }
      });
      console.log(pro_actual)
      const valorPorct = (pro_actual.memoInicial.valor * 100) / listParticiones[index].tam.valor;
      div.style.height = valorPorct + "%";
      document.getElementById("particion_" + listParticiones[index].id).appendChild(div2);

      proceso.particion = listParticiones[index].id;

    }
}
  return part;

}

//Funcion para crear el checkBox de un proceso
function crearListaProcesos(proceso) {
  const contenedorProcesos = document.getElementById("listaDeLosProcesos");

  const divBoton = document.createElement("div");
  divBoton.setAttribute("class", "checkbox-JASoft");

  const inputBoton = document.createElement("input");
  inputBoton.setAttribute("id", "check_" + proceso.pid);
  inputBoton.setAttribute("onclick", "addProceso(id)");
  inputBoton.setAttribute("type", "checkbox");
  inputBoton.setAttribute("value", "Valor");

  const labelBoton = document.createElement("label");
  labelBoton.setAttribute("for", "check_" + proceso.pid);
  labelBoton.innerText = "---";

  const pProceso = document.createElement("p");
  pProceso.innerText = proceso.nombre;

  divBoton.appendChild(inputBoton);
  divBoton.appendChild(labelBoton);

  contenedorProcesos.appendChild(divBoton);
  contenedorProcesos.appendChild(pProceso);
  /*
  Ejemplo del resultado de un CheckBox

  <div class="checkbox-JASoft">
    <input onclick="addProceso(id)" type="checkbox" id="p4_check" value="Valor" />
    <label for="p4_check">---</label>
  </div>
  <p>Paint</p>
   */
}

//Se crean los proceso de acuerdo a la lista
listProceCreate.forEach((element) => {
  crearListaProcesos(element);
});

//Funcion para agregar un proceso a ejecucion
function addProceso(id) {
  var isChecked = document.getElementById(id).checked;

  const pid = id.split("_")[1];
  const proceso = getProceso(pid, listProceCreate);

  if (isChecked) {
    addProcessTable(proceso);
    addProcessRam(proceso);
  } else {
    removeProcessTable(proceso);
    removeProcessRam(proceso)
  }

  /*
    //Obtencion del elemento y sus propiedades
    let color = document.getElementById("ram");
    let elementStyle = window.getComputedStyle(color);
    let elementColor = elementStyle.getPropertyValue('color');

    //Cambios de las propiedades del elemento
    if (elementColor === "rgb(255, 0, 0)") {
        document.getElementById("ram").style.color = "green";
    } else {
        document.getElementById("ram").style.color = "red";
    }
    */
}

function setMemePart(id, mem){
  console.log(id,mem)

}

function addMemTable(particion) {
  const tr = document.createElement("tr"); //Se crear una Fila Para la tabla
  //Esa fila tiene un Id, que se compone de la palabra tabla y del pid del proceso. Ejemplo: tabla_p1
  "tablamem_" + particion.id;

  const td = document.createElement("td");
  td.innerHTML = particion.tam.valor/(1024*1024);  //Columna Item
  tr.appendChild(td);

  const td2 = td.cloneNode();
  td2.innerHTML = particion.tam.valor/1024; //Columna Nombre
  tr.appendChild(td2);

  const td3 = td.cloneNode();
  td3.innerHTML = particion.tam.valor; //Columna Tam Disco
  tr.appendChild(td3);


  document.getElementById("memtabla").appendChild(tr);
}

listParticiones.forEach((e)=> addMemTable(e));



function addProcessTable(proceso) {
  const tr = document.createElement("tr"); //Se crear una Fila Para la tabla
  //Esa fila tiene un Id, que se compone de la palabra tabla y del pid del proceso. Ejemplo: tabla_p1
  tr.id = "tabla_" + proceso.pid;

  const td = document.createElement("td");
  td.innerHTML = proceso.pid; //Columna Item
  tr.appendChild(td);

  const td2 = td.cloneNode();
  td2.innerHTML = proceso.nombre; //Columna Nombre
  tr.appendChild(td2);

  const td3 = td.cloneNode();
  td3.innerHTML = proceso.disco.valor; //Columna Tam Disco
  tr.appendChild(td3);

  const td4 = td.cloneNode();
  td4.innerHTML = proceso.text.valor; //Columna Tam codigo
  tr.appendChild(td4);

  const td5 = td.cloneNode();
  td5.innerHTML = proceso.data.valor; //Columna Tam datos ini
  tr.appendChild(td5);

  const td6 = td.cloneNode();
  td6.innerHTML = proceso.bss.valor; //Columna Tam datos sin ini
  tr.appendChild(td6);

  const td7 = td.cloneNode();
  td7.innerHTML = proceso.memoInicial.valor; //Columna Tam inicial

  tr.appendChild(td7);
  document.getElementById("elementosTablaProceso").appendChild(tr);
  /*
  this.pid = pid;
    this.nombre = nombre;
    this.disco = disco;
    this.text = text;
    this.data = data;
    this.bss = bss; */
}

function removeProcessTable(proceso) {
  var idString = "#tabla_" + proceso.pid;
  const tr = document.querySelector(idString);
  tr.remove();
}

function addProcessRam(proceso) {
  let boolSelec = true; 
  let ajuste = document.querySelector('input[name="ajuste"]:checked').value;
  if (ajuste === 'primer'){
    listParticiones.forEach((element) => {

      if (
        element.estado == true &&
        element.tam.valor >= proceso.memoInicial.valor &&
        boolSelec
      ) {
        p = addPart(proceso, listParticiones.indexOf(element));
        const div = document.createElement("div");
        div.id = "particion_memoria_"+p.id;
        div.setAttribute("class", "procesoRam");
        const valorPorct = (proceso.memoInicial.valor * 100) / p.tam.valor;
        div.style.height = valorPorct + "%";
        document.getElementById("particion_" + p.id).appendChild(div);
  
        proceso.particion = p.id;
        p.estado = false;
        boolSelec = false;
      }
    });
  } if (ajuste == 'segundo'){
    let mejor_tam = ram.tamRam;
    let mejor = NaN
    listParticiones.forEach((element) => {
      if (
        element.estado == true &&
        element.tam.valor >= proceso.memoInicial.valor &&
        boolSelec
      ){
        let diferencia = element.tam.valor - proceso.memoInicial.valor;
       
        if (diferencia < mejor_tam){
          mejor_tam = diferencia;
          mejor = element;
        }
      }
    });
    const div = document.createElement("div");
    div.id = "particion_memoria_"+mejor.id;
    div.setAttribute("class", "procesoRam");
    const valorPorct = (proceso.memoInicial.valor * 100) / mejor.tam.valor;
    div.style.height = valorPorct + "%";
    document.getElementById("particion_" + mejor.id).appendChild(div);

    proceso.particion = mejor.id;
    mejor.estado = false;

    boolSelec = false;
  } if (ajuste === 'tercero'){

    let peor_tam = 0;
    let peor = NaN
    listParticiones.forEach((element) => {
      if (
        element.estado == true &&
        element.tam.valor >= proceso.memoInicial.valor &&
        boolSelec
      ){
        let diferencia = element.tam.valor - proceso.memoInicial.valor;
       
        if (diferencia > peor_tam){
          peor_tam = diferencia;
          peor = element;
        }
      }
    });
    const div = document.createElement("div");
    div.id = "particion_memoria_"+peor.id;
    div.setAttribute("class", "procesoRam");
    const valorPorct = (proceso.memoInicial.valor * 100) / peor.tam.valor;
    div.style.height = valorPorct + "%";
    document.getElementById("particion_" + peor.id).appendChild(div);

    proceso.particion = peor.id;
    peor.estado = false;

  }
  
}

function removeProcessRam(proceso) {
  var idString = "#particion_memoria_" + proceso.particion;
  console.log(idString)
  const div = document.querySelector(idString);
  div.remove();

  listParticiones.forEach(element => {
    if(element.id == proceso.particion){
      element.estado = true;
    }
  });
}
