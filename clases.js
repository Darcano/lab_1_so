//Clase del Sistema Opertativo
export class So {
  constructor(kernel, stack, heap, encabezado) {
    this.kernel = kernel;
    this.stack = stack;
    this.heap = heap;
    this.encabezado = encabezado;
  }
}

//Funcion conversoras
export function to_bytes(tam) {
  if (tam.unidad === "MiB") {
    return { unidad: "bytes", valor: tam.valor * 1024 * 1024 };
  } else if (tam.unidad === "KiB") {
    return { unidad: "bytes", valor: tam.valor * 1024 };
  } else if (tam.unidad === "bytes") {
    return { unidad: "bytes", valor: tam.valor };
  }
}

//Funcion conversora Decimal a Hex
export function decToHex(num) {
  return num.toString(16).toUpperCase();
}

//Funcion para agregar ceros
export function addCeros(hex) {
  const longCade = hex.length;
  const cerosFalt = 6 - longCade;
  let ceros = "";
  for (let index = 0; index < cerosFalt; index++) {
    ceros = ceros + "0";
  }

  return ceros + hex;
}

//Clase de proceso
export class Proceso {
  constructor(pid, nombre, text, data, bss, SistemO, particion) {
    this.particion = particion;

    this.pid = pid;
    this.nombre = nombre;
    this.text = to_bytes(text);
    this.data = to_bytes(data);
    this.bss = to_bytes(bss);

    const tamDisco =
      this.text.valor +
      this.data.valor +
      this.bss.valor +
      to_bytes(SistemO.encabezado).valor;
    this.disco = { unidad: "bytes", valor: tamDisco };

    const memoIni = {
      unidad: "bytes",
      valor:
        this.text.valor +
        this.data.valor +
        this.bss.valor +
        to_bytes(SistemO.encabezado).valor +
        to_bytes(SistemO.heap).valor +
        to_bytes(SistemO.stack).valor,
    };
    this.memoInicial = memoIni;
    this.refParti = "";
  }
}

//Clase de la RAM
export class Ram {
  constructor(tamPart, tamRam) {
    this.tamRam = tamRam;
    this.tamPart = tamPart;
    this.cant = to_bytes(tamRam).valor / to_bytes(tamPart).valor;
  }
}

//Clase de la particion
export class Particion {
  constructor(id, tam, estado) {
    this.id = id;
    this.tam = to_bytes(tam);
    this.estado = estado;
  }
}
